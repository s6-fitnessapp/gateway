const {gatewayUrl} = require("./gatewaySettings.js");


let express = require('express');
let app = express();
let jwks = require('jwks-rsa');
const jwt = require("express-jwt");
let jwtCheck = jwt({
    secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: 'https://fitness-app-ws.eu.auth0.com/.well-known/jwks.json'
    }),
    audience: 'https://fitness-gateway-api.com',
    issuer: 'https://fitness-app-ws.eu.auth0.com/',
    algorithms: ['RS256']
});

app.use(jwtCheck);

module.exports = (req, res, next) => {
    getPathInfo(req.path).then((pathInfo) => {
        if (!pathInfo.jwt) {
            return next();
        }
        getIdToken(req).then((idToken) => {
            firebaseAdmin.auth().verifyIdToken(idToken).then((decodedToken) => {
                req.headers['uid'] = decodedToken.uid;
                return next();
            }).catch((e) => {
                console.log(e);
                return res.status(401).json();
            })
        }).catch((e) => {
            console.log(e);
            return res.status(401).json();
        })
    }).catch(() => {
        console.log('404');
        return res.status(404).json();
    });
};
