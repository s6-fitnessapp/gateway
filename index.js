const {gatewayUrl} = require("./gatewaySettings.js");


let express = require('express');
let app = express();
let jwks = require('jwks-rsa');
const port = process.env.PORT || 3000;
const jwtAuthz = require("express-jwt-authz");

let bodyParser = require('body-parser');
const jwt = require("express-jwt");
let jwtCheck = jwt({
    secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: 'https://fitness-app-ws.eu.auth0.com/.well-known/jwks.json'
    }),
    audience: 'https://fitness-gateway-api.com',
    issuer: 'https://fitness-app-ws.eu.auth0.com/',
    algorithms: ['RS256']
});
const checkPermissions = jwtAuthz(["read:messages", "read:appointments"], {
    customScopeKey: "permissions",
    checkAllScopes: true
});

app.use(jwtCheck);
app.use(checkPermissions)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}));

require('dotenv').config();
require('./services/servicesService').routes(app);

app.use(require('cors')({
    origin:  ['http://exercise-api:8080', gatewayUrl],
}));


app.listen(port, () => console.log(`Web gateway is running on ${port}`));
