
const services = require('../services');
let proxy = require('express-http-proxy');

module.exports = {
    getServices: () => {
        return services;
    },
    routes: (app) => {
        services.forEach((service) => {
            console.log(` Service: ${service.name} `);
            service.paths.forEach((path) => {
                    console.log(`Path: ${service.name}${path.path} `);
                    app.all(path.path, proxy(`${service.name}:8080`))

            });
        });
    }
};
